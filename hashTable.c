#include "hashTable.h"

int hashcode(uint8_t h[]) {
    int result = 0;
    for (size_t i = 0; i < 3; i++) {
        result = (result << 8) | h[i];
    }
    return result;
}

void setItem(struct DataItem* item, uint8_t h[6], uint8_t msg[16]){
    memcpy(item->h, h, sizeof(uint8_t)*6);  
    memcpy(item->msg, msg, sizeof(uint8_t)*16);
    item->next = NULL;
}

//search if find return true, else add and return false
bool search(uint8_t h[6], uint8_t msg[16], uint8_t* msg_f) {

   //get the hash 
   int hashIndex = hashcode(h);
   //get current item
   struct DataItem* cur = hashArray[hashIndex];

   if(cur == NULL){
    //no item
    // printf("not found\n");
    struct DataItem *item = (struct DataItem*) malloc(sizeof(struct DataItem));
    setItem(item, h, msg);
    hashArray[hashIndex] = item;
    return false;
   }

    //search 
    //if found, return; else, add
    struct DataItem* pre = NULL;
    while(cur != NULL){
        if(memcmp(cur->h, h, sizeof(uint8_t)*6) == 0){//find
            printf("msg1\n");
            for (int j = 0; j < 16; j++) {
                printf("%02X ", cur->msg[j]);  // Print each byte in hexadecimal format
                msg_f[j] = cur->msg[j];
            }
            // msg_f = cur->msg;
            
            printf("\nh:\n");
            printhash(cur->h);
            printf("\n");
            return true;
        }
        else{
            pre = cur;
            cur = cur->next;
        }
    }

    //add
    // printf("add item\n");
    struct DataItem *item = (struct DataItem*) malloc(sizeof(struct DataItem));
    setItem(item, h, msg);
    pre->next = item;
    return false;
}


