#include "ht48.h"
#include <stdbool.h>
#include <string.h>

#define SIZE 16777216

struct DataItem {
   uint8_t msg[16];
   uint8_t h[6];  
   struct DataItem* next;
};

struct DataItem* hashArray[SIZE];

int hashcode(uint8_t h[]);
void setItem(struct DataItem* item, uint8_t h[6], uint8_t msg[16]);
bool search(uint8_t h[6], uint8_t msg[16], uint8_t* msg_f);
