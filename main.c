#include "smht48.h"


void test_smht48(){
    printf("start smht48:\n");
    uint8_t k1[6] = {0, 1, 2, 3, 4, 5};
    uint8_t m1[6] = {9, 8, 7, 6, 5, 4};

    uint8_t k2[6] = {0xE4, 0x16, 0x9F, 0x12, 0xD3, 0xBA};
    uint8_t m2[6] = {9, 8, 7, 6, 5, 4};

    uint8_t h1[6],h2[6];

    smht48(k1, 6, m1, h1);
    smht48(k2, 6, m2, h2);


    printhash(h1);
    printf("\n");
    printhash(h2);
    printf("\n");
}

void test_keyrec(){

    /*test correctness
    uint8_t k1[6] = {0x00, 0x08, 0x40, 0x00, 0x20, 0x27};
    uint8_t m1[6] = {9, 8, 7, 6, 5, 4};
    uint8_t h1[6];
    smht48(k1, 6, m1, h1);
    printhash(h1);
    printf("\n");
    */


    printf("start test_keyrec:\n");
    keyrec();
    printf("\n");

}

void test_colsearch(){
    printf("start colsearch:\n");
    uint8_t m1[16];
    uint8_t m2[16];
    colsearch(m1, m2);

    printf("msg1\n");
    for (int j = 0; j < 16; j++) {
        printf("%02X ", m1[j]);  // Print each byte in hexadecimal format
    }

    printf("\nmsg2\n");
    for (int j = 0; j < 16; j++) {
        printf("%02X ", m2[j]);  // Print each byte in hexadecimal format
    }
    printf("\n");

}

void test_smht48ef(){
     printf("start smht48ef:\n");
     smht48ef();
     printf("\n");
}

int main(){
    char choice;
    while (1) {
        printf("---------------------------------------\n");
        printf("Choose an option:\n");
        printf("1. test_smht48 \n");
        printf("2. test_keyrec\n");
        printf("3. test_colsearch\n");
        printf("3. test_smht48ef\n");
        printf("Enter your choice (or 'q' to quit): ");
        
        // Read user's choice
        if (scanf(" %c", &choice) != 1) {
            printf("Invalid input\n");
            break;
        }

        // Check if user wants to quit
        if (choice == 'q') {
            printf("Quitting...\n");
            break;
        }

        // Handle user's choice
        switch(choice) {
        case '1':
            test_smht48();
            break;
        case '2':
            test_keyrec();
            break;
        case '3':
            test_colsearch();
            break;
        case '4':
            test_smht48ef();
            break;
        default:
            printf("Invalid choice\n");
     }
    }
    // test_smht48();
    // test_keyrec();
    // test_colsearch();
    // test_smht48ef();
    


    return 0;
}