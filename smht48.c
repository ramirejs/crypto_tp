#include "smht48.h"



void smht48(const uint8_t k[static 6], uint64_t blen, const uint8_t m[blen], uint8_t h[static 6]){
    uint64_t num = 6+blen;
    uint8_t m_new[num];

    for(uint64_t i=0; i<num; ++i){
        if(i<blen){
            m_new[i] = m[i];
        }
        else{
            m_new[i] = k[i-6];
        }
       
    }
    ht48(num, m_new, h);
}


void keyrec(){
    // uint8_t k[static 6];
    uint8_t msg[6] = {9, 8, 7, 6, 5, 4};
    uint8_t tag[6] = {0x7D, 0x1D, 0xEF, 0xA0, 0xB8, 0xAD};
    // uint8_t tag[6] = {0xC9, 0x1B, 0x96, 0x07, 0xE3, 0x42};
    uint8_t h[6], key[6];

    // Generate all possible combinations
    for (int i = 0; i < 42; i++) {
        for (int j = i + 1; j < 43; j++) {
            for (int k = j + 1; k < 44; k++) {
                for (int l = k + 1; l < 45; l++) {
                    for (int m = l + 1; m < 46; m++) {
                        for (int n = m + 1; n < 47; n++) {
                            for (int o = n + 1; o < 48; o++) {
                                uint64_t num = (1ULL << i) | (1ULL << j) | (1ULL << k) | (1ULL << l) | (1ULL << m) | (1ULL << n) | (1ULL << o);
                                for (int p = 0; p<6; ++p) {
                                    key[p] = (uint8_t)(num & 0xFF);// extracts the lowest 8 bits of num, masking out all higher bits. 
                                    num >>= 8;  // Shift the number 8 bits to the right
                                }
                                smht48(key, 6, msg, h);
                                if(memcmp(h, tag, sizeof(h)) == 0){
                                    printf("k:");
                                    for (int j = 0; j < 6; j++) {
                                    printf("%02X ", key[j]);  // Print each byte in hexadecimal format
                                    }
                                    return ;
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    printf("Found nothing\n");


}


void colsearch(uint8_t msg1[], uint8_t msg2[]){
    // void tcz48_dm(const uint8_t m[static 16], uint8_t h[static 6])
    uint8_t m[16];

    //initialize h
    uint8_t h[6];
    h[0] = IVB0;
    h[1] = IVB1;
    h[2] = IVB2;
    h[3] = IVB3;
    h[4] = IVB4;
    h[5] = IVB5;

    

    bool is_collison;

    //generate msg
    // printf("generate message:\n");
    uint64_t m1 = 0;
    uint64_t m2 = 0;

    for (int p = 0; p<16; ++p) {
        m[p] = 0;
    }
    // h = h_init;
    tcz48_dm(m, h);
    search(h,m, msg1);

    while(m2 <= UINT64_MAX){
        if(m1 <= UINT64_MAX){
            m1++;
            uint64_t temp = m1;
            // printf("m1:%llu\n", m1);
            for (int p = 0; p<8; ++p) {
                 m[p] = (uint8_t)(temp & 0xFF);// extracts the lowest 8 bits of num, masking out all higher bits. 
                 temp >>= 8;
            }
        }
        else{
            ++m2;
            uint64_t temp = m2;
            for (int p = 8; p<16; ++p) {
                 m[p] = (uint8_t)(temp & 0xFF);// extracts the lowest 8 bits of num, masking out all higher bits. 
                 temp >>= 8;
            }
        }
        h[0] = IVB0;
        h[1] = IVB1;
        h[2] = IVB2;
        h[3] = IVB3;
        h[4] = IVB4;
        h[5] = IVB5;
        tcz48_dm(m, h);

        is_collison = search(h, m, msg1);
        if(is_collison){
            //  msg2 = m;
             printf("msg2:\n");
             for (int j = 0; j < 16; j++) {
                    printf("%02X ", m[j]);  // Print each byte in hexadecimal format
                    msg2[j] = m[j];
             }
             printf("\nh:\n");

             printhash(h);
             printf("\n");

        return;
        }
    
    }

}

void smht48ef(){
    //generate k
    uint64_t k = 0;
    srand(time(NULL));
    k = k + (rand() % UINT64_MAX);
    // printf("%02llX", k);
    uint8_t key[6];
    for (int i = 0; i<6; ++i) {
        key[i] = (uint8_t)(k & 0xFF);// extracts the lowest 8 bits of num, masking out all higher bits. 
        k >>= 8;  // Shift the number 8 bits to the right
    }

    //get m1 and m2
    uint8_t m1[16];
    uint8_t m2[16];
    uint8_t h1[6],h2[6];
    colsearch(m1, m2);

    //abtain a collision in smht48 
    smht48(key, 16, m1, h1);
    smht48(key, 16, m2, h2);
    printf("collision:(h1=h2):\n");
    printf("h1:");
    printhash(h1);
    printf("\nh2:");
    printhash(h2);
    printf("\n");


}