// #include "ht48.h"
#include "hashTable.h"
#include "time.h"

void smht48(const uint8_t k[static 6], uint64_t blen, const uint8_t m[blen], uint8_t h[static 6]);
void keyrec();
void colsearch(uint8_t msg1[], uint8_t msg2[]);
void smht48ef();